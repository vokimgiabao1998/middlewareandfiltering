using Microsoft.AspNetCore.Hosting;
using Middleware_Filtering;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton(typeof(ILogger), typeof(Logger<Program>));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
};


app.Map("/level1", level1App =>
{
    level1App.Map("/level2a", level2AApp =>
    {
        level2AApp.Run(async (context) =>
        {
            await context.Response.WriteAsync("level2a1");
        });
    });
});
//app.UseMiddleware<CustomExceptionMiddleware>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();



app.Run();
