﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace Middleware_Filtering
{
    public class IgnoreAttribue: TypeFilterAttribute
    {
        public IgnoreAttribue(): base(typeof(IgnoreActionFilter)) {}

        private class IgnoreActionFilter : IActionFilter
        {
            private readonly ILogger _logger;

            public IgnoreActionFilter(ILoggerFactory logger)
            {
                _logger = logger.CreateLogger<IgnoreAttribue>(); ;
            }

            public void OnActionExecuted(ActionExecutedContext context)
            {
                _logger.LogInformation($" Filter Atrribute - {MethodBase.GetCurrentMethod()}");
            }

            public void OnActionExecuting(ActionExecutingContext context)
            {
                _logger.LogInformation($" Filter Atrribute - {MethodBase.GetCurrentMethod()}");
            }
        }

    }
}
